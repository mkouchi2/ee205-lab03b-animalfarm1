###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author Matthew Kouchi <mkouchi2@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   28_JAN_2021
###############################################################################

TARGET = animalfarm

CC = cc
CFLAGS = -g

SRC = main.c cat.c animals.c
OBJ = $(SRC:.c=.o)
HDR = $(SRC:.c=.h)animals.h cat.h

all: $(TARGET)

$(TARGET): $(OBJ) 
	$(CC) -o $(TARGET) $(OBJ) $(CFLAGS)

.c.o:$(HDR)
	$(CC) -c $<  $(CFLAGS)
	
clean:
	rm -f $(OBJ) $(TARGET).o $(TARGET)
