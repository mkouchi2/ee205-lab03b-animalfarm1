///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file cat.h
/// @version 1.0
///
/// Exports data about cats
///
/// @author Matthew Kouchi <mkouchi2@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   28_JAN_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include<stdbool.h>
#include "animals.h"

/// Declare a enum for cat breeds
enum CatBreeds {MAIN_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};

/// Declare a struct Cat here

struct Cat 
{
 char name[30];
 enum Gender gender;
 enum CatBreeds breed;
 bool isFixed;
 float weight;
 enum Color collar1_color;
 enum Color collar2_color;
 long license;

};

char* printBreed (enum CatBreeds breed);

void addAliceTheCat(int i) ;

void printCat(int i) ;

